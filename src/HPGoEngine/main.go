package main 

import (
	"github.com/gin-gonic/gin"
	"time"
)

func main() {
	r := gin.Default()
	r.Static("/assets", "./assets")
	r.LoadHTMLGlob("templates/*")
	
	r.GET("/index", func(c *gin.Context) {
		obj := gin.H{"title": "Main website at " + time.Now().Format("15:04:05") }
        c.HTML(200, "index.html", obj)
	})
	
	r.GET("/string", func(c *gin.Context) {
        c.String(200, "string content")
	})
	
	r.GET("/json", func(c *gin.Context) {
        c.JSON(200, gin.H{"message0": "json content0", "message1": "json content1", "status": 200})
	})
	
	r.Run(":8000")
}